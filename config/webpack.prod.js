const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const devConfig = require('./webpack.dev');

module.exports = Object.assign({}, devConfig, {
    mode: 'production',
    devtool: 'source-map',
    optimization: {
        splitChunks: {
          chunks: 'all',
        },
        runtimeChunk: true,
        minimizer: [new UglifyJsPlugin({
            sourceMap: true
        })],
    }
  });