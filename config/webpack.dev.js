const path = require('path');

module.exports = {
    mode: 'development',
    devtool: 'cheap-module-source-map',
    entry: {
      main: './src/index.js',
    },
    output: {
      chunkFilename: '[name].js',
      filename: '[name].js'
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
      },
      runtimeChunk: true,
    },
    resolve: {
      modules: [path.resolve(__dirname, '../src'), 'node_modules'],
      alias: {
        '~': path.resolve(__dirname, '../src')
      },
      extensions: ['.js', '.jsx']
    },
    module: {
      strictExportPresence: true,
      rules: [
        { parser: { requireEnsure: false } },
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
          }
        },
        { test: /\.css$/, use: [{
          loader: 'file-loader',
          options: { name: '[name].[ext]' }
        }] },
      ],
    },
    plugins: [],
    node: {
      dgram: 'empty',
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
      child_process: 'empty',
    },
  };