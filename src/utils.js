export function strerror() {
  return "error";
}

export function decode(component) {
  try {
    return decodeURIComponent(component);
  } catch (_err) {
    return '';
  }
}