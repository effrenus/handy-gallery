import { combineReducers } from "redux";
import {
  FETCH_IMAGES_SUCCESS,
  FETCH_IMAGES_FAILURE,
  SET_IMAGE_DETAILS
} from "~/actions";

const defaultState = {
  isFetching: false,
  searchTerm: "",
  currentPage: 1,
  itemsPerPage: 15,
  items: [],
  allImagesCount: 0,
  selectedImageDetail: null
};

function gallery(state = defaultState, { type, ...payload }) {
  switch (type) {
    case FETCH_IMAGES_SUCCESS:
      return { ...state, ...payload };
    case SET_IMAGE_DETAILS:
      return { ...state, selectedImageDetail: payload };
    case FETCH_IMAGES_FAILURE:
      return state;
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  gallery
});

export default rootReducer;
