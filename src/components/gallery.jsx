import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Thumb from "~/components/thumb";
import Paginator from "~/components/paginator";
import { fetch, searchByTitle } from "~/actions";
import { decode } from "~/utils";

// TODO: Move to utils(?)
function parseUrlSearch(str) {
  return str
    .replace(/^\?/, "")
    .split("&")
    .filter(Boolean)
    .map(v => v.split("="))
    .reduce((acc, v) => ((acc[v[0]] = decode(v[1])), acc), {});
}

export class Gallery extends React.Component {
  onSearch(event) {
    const {
      itemsPerPage,
      currentPage,
      searchTerm,
      history,
      dispatch
    } = this.props;
    const newSearchTerm = event.target.value;
    const newPage = newSearchTerm !== searchTerm ? 1 : currentPage;
    history.push(
      `${history.location.pathname}?page=${newPage}&title=${newSearchTerm}`
    );
    dispatch(searchByTitle(newSearchTerm, { page: newPage, itemsPerPage }));
  }
  componentDidMount() {
    const searchParams = parseUrlSearch(window.location.search);
    const page = searchParams.page ? parseInt(searchParams.page) : 1;
    this.props.dispatch(
      fetch({
        title: searchParams.title,
        page,
        itemsPerPage: this.props.itemsPerPage
      })
    );
  }
  nextPage(page) {
    if (!page || Number.isNaN(page)) return;
    const searchParams = parseUrlSearch(window.location.search);
    this.props.dispatch(
      fetch({
        title: searchParams.title,
        page,
        itemsPerPage: this.props.itemsPerPage
      })
    );
  }
  shouldComponentUpdate(nextProps) {
    return [
      "images",
      "currentPage",
      "allImagesCount",
      "searchTerm",
      "itemsPerPage"
    ].some(prop => this.props[prop] !== nextProps[prop]);
  }
  render() {
    const {
      allImagesCount,
      items,
      itemsPerPage,
      currentPage,
      searchTerm,
      history
    } = this.props;
    const pageCount = Math.ceil(allImagesCount / itemsPerPage);
    return (
      <div>
        <input
          type="text"
          placeholder="Filter by title "
          value={searchTerm}
          onChange={this.onSearch.bind(this)}
        />
        <ul className="gallery">
          {items.map(imageAttrs => (
            <li key={imageAttrs.id}>
              <Thumb width={150} height={150} {...imageAttrs} />
            </li>
          ))}
        </ul>
        <Paginator
          onPagination={this.nextPage.bind(this)}
          currentPage={currentPage}
          pageCount={pageCount}
          path={history.location.pathname}
          queryParams={{ title: searchTerm }}
        />
      </div>
    );
  }
}

Gallery.propTypes = {
  allImagesCount: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      url: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      thumbnailUrl: PropTypes.string.isRequired
    })
  ),
  itemsPerPage: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  searchTerm: PropTypes.string.isRequired
};

export default connect(({ gallery: { selectedImageDetail, ...rest } }) => ({
  ...rest
}))(Gallery);
