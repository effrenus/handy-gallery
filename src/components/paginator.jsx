import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const noop = () => {};

const PaginatorItem = ({ currentPage, page, queryParams, path }) => (
  <li>
    {currentPage === page ? (
      <span>{page}</span>
    ) : (
      <Link
        data-page={page}
        to={{
          pathname: path,
          search: `page=${page}${Object.entries(queryParams).reduce(
            (a, [k, v]) => a + (v ? "&" + k + "=" + v : ""),
            ""
          )}`
        }}
      >
        {page}
      </Link>
    )}
  </li>
);

PaginatorItem.propTypes = {
  currentPage: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  queryParams: PropTypes.object.isRequired,
  path: PropTypes.string.isRequired
};

const renderMiddle = ({
  showedPageLimit,
  itemParams,
  pageCount,
  currentPage
}) => {
  const pageToShow = pageCount > showedPageLimit ? showedPageLimit : pageCount;
  let start = Math.max(currentPage, 2);
  while (
    start > 2 &&
    currentPage - start <
      pageToShow - 2 - Math.min(pageCount - currentPage, (pageToShow - 2) / 2)
  ) {
    start--;
  }

  return Array.from({ length: Math.max(pageToShow - 2, 0) })
    .map((_, i) => start + i)
    .map(v => <PaginatorItem key={v} page={v} {...itemParams} />);
};

const Paginator = ({
  currentPage,
  pageCount,
  queryParams = {},
  path = "/",
  onPagination = noop
}) => {
  if (!pageCount || pageCount < 2) {
    return null;
  }
  const showedPageLimit = 10; // Max visible page nums in paginator.
  const itemParams = { currentPage, path, queryParams };

  return (
    <ol
      className="pagination"
      onClick={({ target }) => {
        target.tagName === "A" &&
          onPagination(parseInt(target.dataset.page, 10));
      }}
    >
      <PaginatorItem page={1} {...itemParams} />
      {renderMiddle({ showedPageLimit, itemParams, pageCount, currentPage })}
      <PaginatorItem page={pageCount} {...itemParams} />
    </ol>
  );
};

Paginator.propTypes = {
  currentPage: PropTypes.number.isRequired,
  queryParams: PropTypes.object,
  path: PropTypes.string,
  onPagination: PropTypes.func
};

export default Paginator;
