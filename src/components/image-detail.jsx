import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchImageDetail } from "~/actions";

class ImageDetail extends React.Component {
  componentDidMount() {
    const id = parseInt(this.props.match.params.id, 10);
    this.props.dispatch(fetchImageDetail(id));
  }
  render() {
    const { selectedImageDetail, match, history } = this.props;
    const id = parseInt(match.params.id, 10);
    return !selectedImageDetail || selectedImageDetail.id !== id ? null : (
      <article className="card">
        <button className="ctrl-back" onClick={history.goBack}>
          &larr;
        </button>
        <h1 className="card-title">{selectedImageDetail.title}</h1>
        <figure className="card-image">
          <img src={selectedImageDetail.url} width={600} height={600} />
        </figure>
      </article>
    );
  }
}

ImageDetail.propTypes = {
  selectedImageDetail: PropTypes.shape({
    id: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  })
};

export default connect(({ gallery: { selectedImageDetail } }) => ({
  selectedImageDetail
}))(ImageDetail);
