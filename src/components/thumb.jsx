import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export default function Thumb({ id, thumbnailUrl, width, height, title }) {
  return (
    <Link to={`/images/${id}/`}>
      <figure>
        <img src={thumbnailUrl} width={width} height={height} alt="" />
        <figcaption>{title}</figcaption>
      </figure>
    </Link>
  );
}

Thumb.propTypes = {
  id: PropTypes.number.isRequired,
  thumbnailUrl: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired
};
