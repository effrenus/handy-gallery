import api from "~/api";
import { strerror } from "~/utils";

export const FETCH_IMAGES_SUCCESS = "FETCH_IMAGES_SUCCESS";
export const FETCH_IMAGES_FAILURE = "FETCH_IMAGES_FAILURE";
export const SET_IMAGE_DETAILS = "SET_IMAGE_DETAILS";

export function searchByTitle(title, pagination) {
  return dispatch =>
    api.get
      .gallery_images({ title, ...pagination })
      .catch(_err => dispatch(error("error_types.network ")))
      .then(data => dispatch(update(data)));
}

export function fetch(searchQuery) {
  return dispatch =>
    api.get
      .gallery_images(searchQuery)
      .catch(_err => dispatch(error("error_types.network")))
      .then(data => dispatch(update(data)));
}

export function update(payload) {
  return {
    type: FETCH_IMAGES_SUCCESS,
    ...payload
  };
}

export function setImageDetails(payload) {
  return {
    type: SET_IMAGE_DETAILS,
    ...payload
  };
}

export function fetchImageDetail(imageId) {
  return dispatch =>
    api.get
      .image_detail(imageId)
      .catch(_err => dispatch(error("error_types.network")))
      .then(data => dispatch(setImageDetails(data)));
}

export function error(errorType) {
  return {
    type: FETCH_IMAGES_FAILURE,
    text: strerror(errorType)
  };
}
