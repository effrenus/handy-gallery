const API_URL = "https://jsonplaceholder.typicode.com/photos";
const cache = {};

function filterByPage(items, { page, itemsPerPage }) {
  const len = items.length;
  const offset = (page - 1) * itemsPerPage;
  if (offset > len) return [];
  let res = [];
  for (let i = offset; i < len && i < offset + itemsPerPage; i++) {
    res.push(items[i]);
  }
  return res;
}

function filterByTitle(title, images) {
  return title ? images.filter(item => item.title.includes(title)) : images;
}

export default {
  get: {
    gallery_images({ title = "", ...pagination }) {
      let filteredImages = filterByTitle(title, cache.images || []);
      return cache.images
        ? Promise.resolve({
            items: filterByPage(filteredImages, pagination),
            allImagesCount: filteredImages.length,
            currentPage: pagination.page,
            searchTerm: title
          })
        : fetch(API_URL)
            .then(res => res.json())
            .then(json => {
              cache.images = json;
              filteredImages = filterByTitle(title, cache.images || []);
              return {
                items: filterByPage(filteredImages, pagination),
                allImagesCount: filteredImages.length,
                currentPage: pagination.page,
                searchTerm: title
              };
            });
    },
    image_detail(imageId) {
      return Promise.resolve(cache.images.find(photo => photo.id === imageId));
    }
  }
};
