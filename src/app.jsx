import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Gallery from "~/components/gallery";
import ImageDetail from "~/components/image-detail";

export default function App() {
  return (
    <Switch>
      <Redirect exact path="/" to="/images/" />
      <Route exact path="/images/" component={Gallery} />
      <Route path="/images/:id" component={ImageDetail} />
    </Switch>
  );
}
